FROM php:8.0-apache

# php関連
COPY php.ini /usr/local/etc/php/php.ini
COPY --from=composer:1.10 /usr/bin/composer /usr/bin/composer

RUN apt update -y && apt install -y zip unzip

RUN apt install -y libzip-dev zlib1g-dev libpng-dev \
    && docker-php-ext-install zip gd

# apache関連
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite
