# php-excel

PhpSpreadsheetを使ってみる

## 記事
[PhpSpreadsheetをいろいろ試してみる](https://scrapbox.io/tsuchinaga/PhpSpreadsheet%E3%82%92%E3%81%84%E3%82%8D%E3%81%84%E3%82%8D%E8%A9%A6%E3%81%97%E3%81%A6%E3%81%BF%E3%82%8B)

## 成果物

* hello world: [hello_world.php](./hello_world.php)
* hello world 2: [hello_world2.php](./hello_world2.php)
* 九九: [9x9.php](./9x9.php)
* 見出しのある表: [header.php](./header.php)
* ランダムな値の表: [random_value.php](./random_value.php)
* 集大成の折れ線グラフ付きの表: [graph.php](./graph.php)
