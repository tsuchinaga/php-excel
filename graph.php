<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$rowHeader = ["山田", "田中", "鈴木", "佐藤"];
$sheet->fromArray($rowHeader, null, $sheet->getCellByColumnAndRow(2, 1)->getCoordinate());
$colHeader = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
$sheet->fromArray(array_chunk($colHeader, 1), null, $sheet->getCellByColumnAndRow(1, 2)->getCoordinate());

for ($i = 0; $i < count($rowHeader); $i++) {
    for ($j = 0; $j < count($colHeader); $j++) {
        $sheet->getCellByColumnAndRow(2 + $i, 2 + $j)?->setValue(rand(0, 100));
    }
}

$dataSeriesLabels = [];
foreach ($rowHeader as $i => $head) {
    $dataSeriesLabels[] = new DataSeriesValues(
        dataType: DataSeriesValues::DATASERIES_TYPE_STRING,
        dataSource: getCellCoordinate($sheet, 2 + $i, 1),
        pointCount: 1,
    );
}

$xAxisTickValues = [
    new DataSeriesValues(
        dataType: DataSeriesValues::DATASERIES_TYPE_STRING,
        dataSource: getRangeCoordinate($sheet, 1, 2, 1, 1 + count($colHeader)),
        pointCount: count($colHeader),
    ),
];

$dataSeriesValues = [];
foreach ($rowHeader as $i => $head) {
    $dataSeriesValues[] = new DataSeriesValues(
        dataType: DataSeriesValues::DATASERIES_TYPE_NUMBER,
        dataSource: getRangeCoordinate($sheet, 2 + $i, 2, 2 + $i, 1 + count($colHeader)),
        pointCount: count($colHeader)
    );
}

$series = new DataSeries(
    plotType: DataSeries::TYPE_LINECHART,
    plotGrouping: DataSeries::GROUPING_STANDARD,
    plotOrder: range(0, count($dataSeriesValues) - 1),
    plotLabel: $dataSeriesLabels,
    plotCategory: $xAxisTickValues,
    plotValues: $dataSeriesValues,
);

$chart = new Chart(
    name: "chart1",
    title: new Title(caption: "Test Stacked Line Chart"),
    legend: new Legend(position: Legend::POSITION_TOPRIGHT),
    plotArea: new PlotArea(plotSeries: [$series]),
    displayBlanksAs: DataSeries::EMPTY_AS_GAP,
);

$chart->setTopLeftPosition("A7");
$chart->setBottomRightPosition("H20");
$sheet->addChart($chart);

header("Content-Disposition: attachment; filename=\"graph.xlsx\"");
header('Cache-Control: max-age=0');

$writer = new Xlsx($spreadsheet);
$writer->setIncludeCharts(true);
$writer->save('php://output');

function getCellCoordinate(Worksheet $sheet, int $col, int $row): string
{
    return $sheet->getCodeName() . "!" . $sheet->getCellByColumnAndRow($col, $row)->getCoordinate();
}

function getRangeCoordinate(Worksheet $sheet, int $startCol, int $startRow, int $endCol, int $endRow): string
{
    return $sheet->getCodeName() . "!" . $sheet->getCellByColumnAndRow($startCol, $startRow)->getCoordinate() . ":" . $sheet->getCellByColumnAndRow($endCol, $endRow)->getCoordinate();
}
