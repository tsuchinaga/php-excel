<?php

require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
foreach (str_split("Hello World !") as $i => $s) {
    $sheet->getCellByColumnAndRow($i + 1, 1)?->setValue($s);
}

header("Content-Disposition: attachment; filename=\"HelloWorld2.xlsx\"");
header('Cache-Control: max-age=0');

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
