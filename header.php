<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$rowHeader = ["日", "月", "火", "水", "木", "金", "土"];
$sheet->fromArray($rowHeader, null, $sheet->getCellByColumnAndRow(2, 1)->getCoordinate());
$colHeader = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$sheet->fromArray(array_chunk($colHeader, 1), null, $sheet->getCellByColumnAndRow(1, 2)->getCoordinate());

$i = 0;
$row = 0;
while (true) {
    $day = date("Y-m-d", strtotime("+$i day", strtotime("2021-01-01")));
    $w = (int)date("w", strtotime($day));
    $sheet->getCellByColumnAndRow($w + 2, 2 + $row)?->setValue($day);
    $i++;
    if ($w === 6) $row++;
    if ($row >= 10) break;
}

header("Content-Disposition: attachment; filename=\"header.xlsx\"");
header('Cache-Control: max-age=0');

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
